const axios = require('axios')
require('dotenv').config({ path: '.env' })
module.exports = axios.create({
    baseURL: process.env.BASE_URL
})