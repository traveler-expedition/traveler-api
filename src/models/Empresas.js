const mongoose = require('mongoose');

const keySchema = mongoose.Schema({
  cnpj: {
    type: String,
    required: true,
  },
  key: {
    type: String,
    required: true,
  },
});
const schema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  keys: [keySchema],
});

module.exports = mongoose.model('Empresas', schema, 'empresas');
