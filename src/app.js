const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const Queue = require("./configs/queue");
const BullBoard = require("bull-board");
const router = require("./routes/routes");
const connectDB = require("./configs/database");
const dbUser = process.env.DB_USER;
const dbName = process.env.DB_NAME;
const dbPassword = process.env.DB_PASSWORD;
const cors = require("cors");
connectDB(
  `mongodb+srv://${dbUser}:${dbPassword}@cluster0.hk58y.gcp.mongodb.net/${dbName}?retryWrites=true&w=majority`
);
const app = express();

BullBoard.setQueues([new BullBoard.BullAdapter(Queue)]);

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, "public")));
app.use("/admin/queues", BullBoard.router);

app.use(router);

module.exports = app;
