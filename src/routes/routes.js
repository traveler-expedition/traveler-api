const notasController = require("../controllers/notasController");

const notasEnviadasController = require("../controllers/notasEnviadasController");
const userController = require("../controllers/userController");
const empresasCotroller = require("../controllers/empresaController");
const {
  verifyUserAlreadExists,
  veryfyIfUserExists,
  veryfyIfCompanyExists,
} = require("../utils/midwares");
const express = require("express");

const router = express.Router();

router.get("/notas", notasController.show);
router.get("/notas/:id", notasController.getOne);
router.post("/notas", notasController.create);
router.put("/notas", notasController.update);

router.get("/enviadas", notasEnviadasController.show);

router.post("/users", verifyUserAlreadExists, userController.create);
router.post("/login", userController.login);
router.put("/users/:id", veryfyIfUserExists, userController.edit);

router.post("/company", empresasCotroller.create);
router.put("/company/:id", veryfyIfCompanyExists, empresasCotroller.edit);
router.get("/company/:id", veryfyIfCompanyExists, empresasCotroller.getOne);

module.exports = router;
