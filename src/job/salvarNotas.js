const request = require('../configs/configAxios');
const url = require('../configs/configsURL');
const { getDate, getKeys } = require('./consultaDb');
const NotasFiscais = require('../models/NotasFiscais');
const logger = require('../configs/logger')
// função que faz a requisição para a api externa e retorna os dados
module.exports = {
  key: 'Salvar Notas',
  async handle({data}) {
  
    const result = await getDate();
    const keys = await getKeys(data.empresaId)

    async function call(data, keys) {
      const requests = []
      for (const i in keys) {
        requests.push(await getNfs(data, keys[i].key))
      }
      Promise.allSettled(requests);
    }

    async function getNfs(dataInicial, apikey) {
      request.get(`page=${url.page}/json/&filters=dataEmissao[${dataInicial} TO ${new Date().toLocaleString()}]; ${url.situation}&apikey=${apikey}`)
        .then(async (response) => {

          if (response.data.retorno.notasfiscais) {
            await saveData(response.data.retorno.notasfiscais);
          } else {
            return;
          }
        })
        .catch((erro) => {
          logger.error(erro)
          if (erro.response.status === 403 || erro.response.status === 401) {
            
            logger.warn(`Api Key ${apikey} Desatualizada, favor entrar em contato com o Admin`);
          }
        });
    }


    async function saveData(dados) {
      
      const notas = dados.map((i) => {
        return {
          chave_acesso: i.notafiscal.chaveAcesso,
          numero_nota: i.notafiscal.numero,
          serie: i.notafiscal.serie,
          data_emissao: i.notafiscal.dataEmissao,
          pedido_loja: i.notafiscal.numeroPedidoLoja,
          status: i.notafiscal.situacao,
          cnpj: i.notafiscal.chaveAcesso.slice(6, 20),
          link_danfe: i.notafiscal.linkDanfe,
        };
      });
      try {
        await NotasFiscais.create(notas).then(() => {
          url.page++;

          call(result, keys);
        });
      } catch (erro) {
        logger.error(erro);
      }
    }

    call(result, keys);

  },
};
