const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Users = require('../models/Users');
const controller = {};

controller.create = async (req, res) => {
  const { login, name, password, role, company } = req.body;

  if (!login) {
    return res.status(400).json({ error: 'you must inform the login' });
  }

  if (!name) {
    return res.status(400).json({ error: 'you must inform the user name' });
  }
  if (!password) {
    return res.status(400).json({ error: 'you must inform the password' });
  }

  if (!role) {
    return res.status(400).json({ error: 'you must inform the role' });
  }

  const encriptedPsw = await bcrypt.hash(password, 10);

  try {
    const user = await Users.create({
      login,
      name,
      password: encriptedPsw,
      created_at: new Date().toISOString(),
      role,
      status: 'ativo',
      company,
    });

    const token = jwt.sign(
      {
        user_id: user._id,
        login,
        role: user.role,
        status: user.status,
        company: user.company,
      },
      process.env.TOKEN_KEY,
      {
        expiresIn: '2h',
      },
    );

    user.token = token;
    return res.status(201).json(user);
  } catch (error) {
    return res.status(500).json(error);
  }
};

controller.edit = async (req, res) => {
  const user = req.user;

  const updatedUser = await Users.findByIdAndUpdate(user._id, req.body);

  return res.status(201).json(updatedUser);
};

controller.login = async (req, res) => {
  const { login, password } = req.body;

  if (!login) {
    return res.status(400).json({ error: 'login is required' });
  }

  if (!password) {
    return res.status(400).json({ error: 'password is required' });
  }

  try {
    const user = await Users.findOne({ login });

    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        {
          user_id: user._id,
          login,
          role: user.role,
          status: user.status,
          company: user.company,
        },
        process.env.TOKEN_KEY,
        {
          expiresIn: '24h',
        },
      );

      user.token = token;
      return res.status(200).json(user);
    }
    return res.status(400).json({ error: 'Invalid Credentials' });
  } catch (error) {
    return res.status(500).json(error);
  }
};

controller.logout = async (req, res) => {
  const token = req.headers['x-access-token']
  jwt.sign(token, "", { expiresIn: 1 }, (logout, err) => {

    if (logout) {
      res.status(201).json({ message: 'You have been Logged Out' })
    } else {
      res.status(500).json({ error: "Somenthing went wrong" })
      console.log(err)
    }
  })

}

controller.show = async (req, res) => {
  try {

    const users = await Users.find({}, { _id: 1, login: 1, name: 1, created_at: 1, role: 1, status: 1, company: 1 })


    return res.status(200).json(users)

  } catch (error) {
    return res.status(500).json(error)

  }


}

module.exports = controller;
