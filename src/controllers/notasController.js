const Queue = require('../configs/queue');
const NotasFiscais = require('../models/NotasFiscais');
const controller = {};
const jwt = require('jsonwebtoken')

controller.show = async (req, res) => {
  try {
    const notas = await (await NotasFiscais.find())
    return res.json(notas).status(200);
  } catch (erro) {
    return res.status(500).json(erro);
  }
};

controller.getOne = async (req, res) => {
  const notaId = req.params.id;

  const nota = await NotasFiscais.findById(notaId);
  res.json(nota).status(200);
};

controller.create = async (req, res) => {
  const token = req.headers['x-access-token']
  const decoded = jwt.decode(token)
  try {
    await Queue.add({empresaId:decoded.company});
    res.status(201).end();
  } catch (erro) {
    return res.status(500).json(erro);
  }
};

controller.update = async (req, res) => {
  try {
    const numNota = req.body.numero;
    const nota = await NotasFiscais.findOneAndUpdate(
        {numero_nota: numNota},
        {status: 'Enviado'},
    );
    res.json(nota).status(201);
  } catch (erro) {
    return res.status(500).json(erro);
  }
};

module.exports = controller;
