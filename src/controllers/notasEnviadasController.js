const NotasFiscais = require('../models/NotasFiscais');

const controller = {};

controller.show = async (req, res) =>{
  const notasEnviadas = await NotasFiscais.find({status: 'Enviado'});

  res.json(notasEnviadas).status(200);
};

module.exports = controller;
