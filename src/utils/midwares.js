const Empresas = require('../models/Empresas');
const Users = require('../models/Users');
const jwt = require('jsonwebtoken');
const config = process.env;

module.exports = {
  async verifyToken(req, res, next) {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (!token) {
      return res.status(403).json({
        error: 'A token is required to acces the page',
      });
    }

    try {
      const decoded = jwt.verify(token, config.TOKEN_KEY);
      req.user = decoded;
    } catch (error) {
      return res.status(401).json({error: 'Invalid Token'});
    }

    return next();
  },

  async verifyUserAlreadExists(req, res, next) {
    const {login} = req.body;

    const user = await Users.findOne({login});


    if (user) {
      return res.status(409).send('User Already Exist. Please Login');
    }
    return next();
  },

  async veryfyIfUserExists(req, res, next) {
    const _id = req.params;
    
    try {
      const user = await Users.findOne(_id);

      if (!user) {
        return res.status(404).json({error: 'User does not exists'});
      }
      req.user = user;
    } catch (error) {
      console.log(error);
    }

    return next();
  },

  async veryfyIfCompanyExists(req, res, next) {
    const {id} = req.params;
      
    const company = await Empresas.findById(id);
    if (!company) {
      return res.status(404).json({error: 'company not found'});
    }

    req.company = company;
    return next();
  },

  async verifyIfUserIsAdm(req, res, next) {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    const decodeToken = jwt.decode(token)

    if(decodeToken.role !== 'admin'){
      return res.status(401).json({error:'Access denied'})
    }

    return next()
  }
};
